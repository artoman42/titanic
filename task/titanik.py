import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    titles = ["Mr. ", "Mrs. ", "Miss. "]
    median_age_by_title = {}

    for title in titles:
        median_age = df[df['Name'].str.contains(title)]['Age'].median()
        median_age_by_title[title] = median_age

    result = []

    for title in titles:
        missing_values = df.loc[(df['Name'].str.contains(title))]['Age'].isnull().sum()
        median_age = median_age_by_title[title]
        result.append((title[:-1], missing_values, round(median_age)))

    return result